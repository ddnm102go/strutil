package strutil_test

import (
	"testing"

	"gitlab.com/ddnm102go/strutil"
)

func TestString(t *testing.T) {
	s := strutil.String(10)
	if s == "" {
		t.Fail()
	}
}

func TestSpaceRemover(t *testing.T) {
	var tt = []struct {
		name   string
		value  string
		result string
	}{
		{name: "1 line", value: "   this is    a line     with   many     space     ", result: "this is a line with many space"},
		{name: "multi line", value: `    
		this is a    line with      special charactor		on multi  		line    `, result: "this is a line with special charactor on multi line"},
		{name: "multi line 2", value: `    
		this is a    line with      special charactor 			on multi  		line    `, result: "this is a line with special charactor on multi line"},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			rs := strutil.SpacesRemover(tc.value)
			if rs != tc.result {
				t.Fatalf("expected: %q; got: %q", tc.result, rs)
			}
		})
	}
}
