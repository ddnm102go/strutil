package strutil

import (
	"math/rand"
	"strings"
	"time"
)

const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

// RandSeq create a random string
func RandSeq(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

// StringWithCharset create random string with charset
func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// String create random string
func String(length int) string {
	return StringWithCharset(length, charset)
}

// SpacesRemover remove unnecessary space
// It will replace \t thanh space and remove another \t if unnecessary
// remove \n, \r
// Ex: "  my name   is    dan  " => "my name is dan"
func SpacesRemover(s string) string {
	var allowDelete = false
	s = strings.TrimSpace(s) // cắt bỏ khoảng trắng ở đầu và cuối chuỗi `s`
	var ns = ""
	for _, r := range s {
		// gặp khoảng trắng lần đầu tiên thì không `bỏ qua - continue` mà set allowDelete = true
		// gặp khoảng trắng từ lần 2 trở đi thì `bỏ qua` (vì allowDelete = true)
		// gặp ký tự (khác khoảng trắng thì set allowDelete = false)
		if r == ' ' {
			if allowDelete {
				continue // skip this rune
			}
			allowDelete = true
		} else if r == '\t' {
			// thay the \t thanh khoang trang neu gap \t lan dau tien
			if allowDelete {
				continue
			}
			r = ' '
			allowDelete = true
		} else if r == '\n' || r == '\r' {
			// remove cac ky tu dac biet \n, \r
			continue
		} else {
			allowDelete = false
		}
		ns += string(r)
	}
	return ns
}

// SpacesTrimAll will remove all space in string s
// it is also remove /t, /r, /n
func SpacesTrimAll(s string) string {
	s = strings.TrimSpace(s) // cắt bỏ khoảng trắng ở đầu và cuối chuỗi `s`
	var ns = ""
	for _, r := range s {
		// gặp khoảng trắng thì bỏ qua
		if r == ' ' || r == '\t' || r == '\n' || r == '\r' {
			continue // skip this rune
		}
		ns += string(r)
	}
	return ns
}

// Replaces will replace each 'old' string with 'new' string in 's' string
func Replaces(s string, new string, n int, old ...string) string {
	for _, oldx := range old {
		s = strings.Replace(s, oldx, new, n)
	}
	return s
}

// Reverse will reverse string input
func Reverse(s string) string {
	var r = ""
	for _, sx := range s {
		r = string(sx) + r
	}
	return r
}
